![Donut and Coffee](Donut_and_Coffee_FINAL.png)

# Donut and Coffee
## A Blender's beginner project

Project based on Blender Guru's [**"Blender Begginner Tutorial Series"**](https://www.youtube.com/playlist?list=PLjEaoINr3zgHs8uzT3yqe4iHGfkCmMJ0P&disable_polymer=true)  
Please feel free to fork this project, inspect it and modify it.  

--------------------------------------

# Donas y Café
## Un proyecto para principiantes en Blender

Proyecto basado en la [**"Serie de Tutoriales para Principiantes de Blender"**](https://www.youtube.com/playlist?list=PLjEaoINr3zgHs8uzT3yqe4iHGfkCmMJ0P&disable_polymer=true) de Blender Guru.  
Sentite libre de forkear este proyecto, inspeccionarlo y modificarlo.
